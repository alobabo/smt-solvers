import subprocess

NB_ROWS = 8
NB_COLS = 8
filename = "queens.txt"
ind1 = "\t"
ind2 = "\t\t"

intro = [ 
        "(declare-fun f (Int Int) Bool)\n",
        "(assert (and\n"
        ]

conclusion = [
        "))\n",
        "(check-sat)\n",
        "(get-model)"]

def pos(x, y): 
    return "(f {} {})".format(x, y)

def min_one_per_row():
    t = ["(and\n"]
    for y in range(NB_ROWS):
        t.append("{}(or".format(ind1))
        for x in range(NB_COLS):
            t.append(" {}".format(pos(x, y)))
        t.append("{})\n".format(ind1))
    t.append(")\n\n")
    return t

def max_one_per_row():
    t = ["(and\n"]
    for y in range(NB_ROWS):
        t.append("{}(and\n".format(ind1))
        for x1 in range(NB_COLS-1):
            for x2 in range(x1+ 1, NB_COLS):
                t.append("{}(not (and {} {}))\n".format(ind2,pos(x1, y), pos(x2,y)))
        t.append("{})\n".format(ind1))
    t.append(")\n")
    return t

def min_one_per_column():
    t = ["(and\n"]
    for x in range(NB_COLS):
        t.append("{}(or".format(ind1))
        for y in range(NB_ROWS):
            t.append(" {}".format(pos(x, y)))
        t.append("{})\n".format(ind1))
    t.append(")\n\n")
    return t

def max_one_per_column():
    t = ["(and\n"]
    for x in range(NB_COLS):
        t.append("{}(and\n".format(ind1))
        for y1 in range(NB_ROWS-1):
            for y2 in range(y1+ 1, NB_ROWS):
                t.append("{}(not (and {} {}))\n".format(ind2,pos(x, y1), pos(x,y2)))
        t.append("{})\n".format(ind1))
    t.append(")\n")
    return t

def max_one_per_diag1():
    t = ["(and\n"]
    for i in range(1, NB_ROWS):
        t.append("{}(and\n".format(ind1))
        for j in range(i):
            x1 = j
            y1 = i-j
            for k in range(j+1, i+1):
                x2 = k
                y2 = i - k
                t.append("{}(not (and {} {}))\n".format(ind2,pos(x1, y1), pos(x2,y2)))
        t.append("{})\n".format(ind1))

    for i in range(1, NB_COLS-1):
        t.append("{}(and\n".format(ind1))
        for j in range(NB_COLS - i):
            x1 = i + j
            y1 = NB_ROWS - j - 1 
            for k in range(j+1, NB_COLS - i):
                x2 = i + k 
                y2 = NB_ROWS - k - 1
                t.append("{}(not (and {} {}))\n".format(ind2,pos(x1, y1), pos(x2,y2)))
        t.append("{})\n".format(ind1))

    t.append(")\n")
    return t

def max_one_per_diag2():
    t = ["(and\n"]
    for i in range(1, NB_ROWS):
        t.append("{}(and\n".format(ind1))
        for j in range(i):
            x1 = NB_ROWS - j - 1
            y1 = i - j
            for k in range(j+1, i+1):
                x2 = NB_ROWS - k - 1
                y2 = i - k
                t.append("{}(not (and {} {}))\n".format(ind2,pos(x1, y1), pos(x2,y2)))
        t.append("{})\n".format(ind1))
    t.append(")\n")

    for i in range(1, NB_ROWS-1):
        t.append("{}(and\n".format(ind1))
        for j in range(NB_ROWS- i):
            x1 = NB_COLS - i - j - 1
            y1 = NB_ROWS - j - 1 
            for k in range(j+1, NB_ROWS - i):
                x2 = NB_COLS - i - k - 1
                y2 = NB_ROWS - k - 1
                t.append("{}(not (and {} {}))\n".format(ind2,pos(x1, y1), pos(x2,y2)))
        t.append("{})\n".format(ind1))

    return t

def prevent_solution(s):
    t = [];
    t.append("{}(not (and".format(ind1))
    for queen in s:
        x,y = queen
        t.append(" {}".format(pos(x,y)))
    t.append("))\n")
    return t


def generate_file(solutions):
    f = open(filename, "w")
    f.writelines(intro);
    f.write("; Min one per row\n")
    f.writelines(min_one_per_row());
    f.write("; Max one per row\n")
    f.writelines(max_one_per_row());
    f.write("; Min one per column\n")
    f.writelines(min_one_per_column());
    f.write("; Max one per column\n")
    f.writelines(max_one_per_column());
    f.write("; Max one per diagonal 1\n")
    f.writelines(max_one_per_diag1());
    f.write("; Max one per diagonal 2\n")
    f.writelines(max_one_per_diag2());
    f.write("; Solutions already found\n")
    for solution in solutions:
        f.writelines(prevent_solution(solution));
    f.writelines(conclusion);
    f.close()


def main():
    solutions = []
    while True:
        generate_file(solutions)
        proc = subprocess.run(
                ["z3", "-smt2", filename], 
            capture_output = True)
        if proc.returncode == 0:
            output = proc.stdout.decode('ascii').splitlines()

            solution = []
            for line in output[3:11]:
                solution.append((int(line[20]), int(line[30])))
            solutions.append(solution)
        else:
            break;

    print("{} solutions found for the 8 Queens problem".format(len(solutions)))


if __name__ == "__main__":
    main()

